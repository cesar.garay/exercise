var mC = {
    subsTabVisible: false
}
var tab = document.getElementsByClassName('og-tab');

for (var i=0; i < tab.length; i++) {
    tab[i].onclick = function($event){
        var subTab = document.getElementById('og-subs-template');
        var principalTab = document.getElementById('og-orders-template');
        for (var i=0; i < tab.length; i++) {
            tab[i].classList.remove('selected');
        }
        $event.target.classList.add('selected');

        if(eval($event.target.dataset.classSelected)) {
            principalTab.style.display='block';
            subTab.style.display= 'none';
        } else {
            principalTab.style.display='none';
            subTab.style.display= 'block';
        }

    }
}

var subRow = document.getElementsByClassName('og-sub-row');
for (var i=0; i < subRow.length; i++) {
        var toggleButton = subRow[i].getElementsByClassName('og-subs-more-changes-toggle');
        subRow[i].setAttribute('data-row-key', i);
        toggleButton[0].setAttribute('data-parent-row-key', i);
        toggleButton[0].onclick = function($event) {
            var parent = document.querySelectorAll('[data-row-key="'+$event.target.dataset.parentRowKey+'"]');
            var changesContainer = parent[0].getElementsByClassName('og-more-changes-container');

            if (changesContainer[0].classList.contains('og-show-more-changes')) {
                changesContainer[0].classList.remove('og-show-more-changes');
                $event.target.classList.remove('open');
            } else {
                changesContainer[0].classList.add('og-show-more-changes');
                $event.target.classList.add('open');
            }

        };
}
